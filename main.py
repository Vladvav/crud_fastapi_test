from pydantic import BaseModel, Field
from fastapi import FastAPI
from crud import CRUD
from pydantic import BaseModel, create_model
from pydantic.fields import FieldInfo
from app.connector import PostgreSQLConnector
from app.routers import Router
from app.crud import create_objects
from datetime import  datetime
from sqlalchemy import Column, Integer, String, DateTime, engine, Date, Time, TIMESTAMP, ForeignKey, text, Float, \
    distinct, or_, \
    not_, func, and_, desc, Enum, Boolean, BigInteger, Sequence, MetaData, asc, create_engine

app = FastAPI()


class UserSchema(BaseModel):
    id: int
    created_at:  datetime = None
    updated_at: datetime = None
    telegram_id: str
    telegram_username: str
    first_name: str = None
    last_name: str = None
    phone: str = None


class UserModel(object):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    created_at = Column(DateTime, default=datetime.now)
    updated_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)
    telegram_id = Column(String)
    telegram_username = Column(String)
    first_name = Column(String)
    last_name = Column(String, nullable=True)
    phone = Column(String, nullable=True)


connector = PostgreSQLConnector(
    connection_string="",
    model=UserModel
)
router = Router(
    prefix='potato',
    tags='potato'

)

app = create_objects(
    schema=UserSchema,
    model=UserModel,
    router=router,
    connector=connector
)
