from fastapi import APIRouter
from typing import (
    Any,
    Callable,
    List,
    Mapping,
    Type,
    Coroutine,
    Optional,
    Union,
)

class Router():
    def __init__(self, prefix=None, tags=None):
        # проверка что префикс не занят
        self.prefix: prefix
        self.tags: tags

    def include_router(self, app, router):
        app.include_router(router)

