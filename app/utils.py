from django.db import models
from pydantic import BaseModel, Field

from fastapi_crudrouter.core._types import PYDANTIC_SCHEMA as SCHEMA
from typing import (
    Any,
    Callable,
    List,
    Mapping,
    Type,
    Coroutine,
    Optional,
    Union,
)
from fastapi_crudrouter.core._utils import AttrDict, get_pk_type
Model = Mapping[Any, Any]
CALLABLE = Callable[..., SCHEMA]
CALLABLE_LIST = Callable[..., List[SCHEMA]]

def create_pydantic_schema(model: Type[models.Model]) -> Type[BaseModel]:
    fields = {
        field.name: (
            field.get_internal_type() if isinstance(field, models.Field) else Any,
            Field(..., alias=field.name)
        )
        for field in model._meta.fields
    }

    # Динамически создаем класс схемы на основе переданной модели Django
    return type(model.__name__ + "Schema", (BaseModel,), fields)


def pydantify_record(
        models: Union[Model, List[Model]]
) -> Union[AttrDict, List[AttrDict]]:
    if type(models) is list:
        return [AttrDict(**dict(model)) for model in models]
    else:
        return AttrDict(**dict(models))  # type: ignore