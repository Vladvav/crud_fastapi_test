from fastapi import FastAPI
from app.utils import create_pydantic_schema
from fastapi_crudrouter.core._types import PYDANTIC_SCHEMA as SCHEMA
from typing import Callable,List
CALLABLE = Callable[..., SCHEMA]
CALLABLE_LIST = Callable[..., List[SCHEMA]]
from app.routers import Router
from app.connector import PostgreSQLConnector, RedisConnector
from app.cruds.database_crud import DatabasesCRUDRouter
from app.cruds.memory_crud import MemoryCRUDRouter
app = FastAPI()

#
# schema    1 1 1 1   0 0 0 0   0
# model     0 0 0 0   1 0 1 0   0
# router    0 1 1 0   0 1 1 0   0
# connector 0 1 0 1   0 1 0 1   1
#


def create_objects(schema=None, model=None, router=None, connector=None):
    # Вариант 1: если схемы нет, а модель есть
    if schema is None and model is not None:
        schema = create_pydantic_schema(model)

    # Вариант 2: если схемы нет и модели нет, но есть коннектор
    if schema is None and model is None and connector is not None:
        model = connector.model()
        schema = create_pydantic_schema(model)

    # Вариант 3: если схема есть
    if schema is not None:
        # если модели нет но есть коннектор
        if model is None and connector is not None:
            model = connector.model()
        if router is not None:
            router = Router(router)

        return crud_generator(schema, model, router, connector)

    return




def crud_generator(schema, model=None, router=None, connector=None):
    if connector is None:
        if router is None:
            # Тут пример memory crud куда надо будет передавать данные
            return MemoryCRUDRouter(
            schema=schema,
            create_schema=schema,
            )
        router.include_router(app, MemoryCRUDRouter(
            schema=schema,
            create_schema=schema,
        ))
        return router

    if router is None:
        # Тут пример database crud куда надо будет передавать данные
        return DatabasesCRUDRouter(
            schema=schema,
            create_schema=schema,
            connector=connector
        )
    router.include_router(app, MemoryCRUDRouter(
        schema=schema,
        create_schema=schema,
        connector=connector
    ))
    return router

